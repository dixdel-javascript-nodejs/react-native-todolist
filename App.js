import AsyncStorage from '@react-native-async-storage/async-storage'
import { useEffect, useState } from 'react'
import { Alert, ScrollView, StyleSheet, View } from 'react-native'
import Dialog from 'react-native-dialog'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context'
import uuid from 'react-native-uuid'
import { ButtonAdd } from './components/ButtonAdd'
import { CardTodo } from './components/CardTodo'
import { Header } from './components/Header'
import { TabBottomMenu } from './components/TabBottomMenu'

let isFirstRender = true
let isUpdatedByLoadTodoList = false
export default function App() {
    const [todoList, setTodoList] = useState([])
    const [selectedTabName, setSelectedTabName] = useState('all')
    const [isAddDialogDisplayed, setIsAddDialogDisplayed] = useState(false)
    const [inputValue, setInputValue] = useState('')

    useEffect(() => {
        loadTodoList()
    }, []) // triggered only once

    useEffect(() => {
        if (!isUpdatedByLoadTodoList) {
            // useEffect runs at least once at launch
            if (!isFirstRender) {
                saveTodoList()
            } else {
                isFirstRender = false
            }
        } else {
            isUpdatedByLoadTodoList = false
        }
    }, [todoList])

    async function loadTodoList() {
        try {
            const todoListString = await AsyncStorage.getItem('@todoList')
            const parsedTodoList = JSON.parse(todoListString)
            isUpdatedByLoadTodoList = true // don't trigger todoList useEffect
            setTodoList(parsedTodoList || [])
        } catch (error) {
            alert(error)
        }
    }

    async function saveTodoList() {
        try {
            await AsyncStorage.setItem('@todoList', JSON.stringify(todoList))
        } catch (error) {
            alert(error)
        }
    }

    function getFilteredList() {
        switch (selectedTabName) {
            case 'all':
                return todoList
            case 'inProgress':
                return todoList.filter(todo => !todo.isCompleted)
            case 'done':
                return todoList.filter(todo => todo.isCompleted)
        }
    }

    function deleteTodo(todoToDelete) {
        Alert.alert(
            'Delete todo',
            'Are you sure you want to delete this todo?',
            [
                {
                    text: 'Delete',
                    style: 'destructive',
                    onPress: () => {
                        setTodoList(
                            todoList.filter(t => t.id !== todoToDelete.id)
                        )
                    },
                },
                { text: 'Cancel', style: 'cancel' },
            ]
        )
    }

    function renderTodoList() {
        return getFilteredList().map(todo => (
            <View key={todo.id} style={styles.cardItem}>
                <CardTodo
                    onPress={updateTodo}
                    onLongPress={deleteTodo}
                    todo={todo}
                />
            </View>
        ))
    }

    function updateTodo(todo) {
        const updatedTodo = {
            ...todo,
            isCompleted: !todo.isCompleted,
        }
        const updatedTodoList = [...todoList]
        const indexToUpdate = updatedTodoList.findIndex(
            t => t.id === updatedTodo.id
        )
        updatedTodoList[indexToUpdate] = updatedTodo
        setTodoList(updatedTodoList)
    }

    function addTodo() {
        const newTodo = {
            id: uuid.v4(),
            title: inputValue,
            isCompleted: false,
        }
        setTodoList([...todoList, newTodo])
        setIsAddDialogDisplayed(false)
        setInputValue('')
    }

    function renderAddDialog() {
        return (
            <Dialog.Container
                visible={isAddDialogDisplayed}
                onBackdropPress={() => setIsAddDialogDisplayed(false)}
            >
                <Dialog.Title>Add todo</Dialog.Title>
                <Dialog.Description>
                    Choose a name for your todo
                </Dialog.Description>
                <Dialog.Input
                    onChangeText={text => setInputValue(text)}
                    placeholder="Ex: Go to the dentist"
                />
                <Dialog.Button
                    label="Cancel"
                    color="grey"
                    onPress={() => setIsAddDialogDisplayed(false)}
                />
                <Dialog.Button
                    disabled={inputValue.length === 0}
                    label="Save"
                    onPress={addTodo}
                />
            </Dialog.Container>
        )
    }

    return (
        <>
            <SafeAreaProvider>
                <SafeAreaView style={styles.app}>
                    <View style={styles.header}>
                        <Header />
                    </View>
                    <View style={styles.body}>
                        <ScrollView>{renderTodoList()}</ScrollView>
                    </View>
                    <ButtonAdd onPress={() => setIsAddDialogDisplayed(true)} />
                </SafeAreaView>
            </SafeAreaProvider>
            <View style={styles.footer}>
                <TabBottomMenu
                    onPress={setSelectedTabName}
                    selectedTabName={selectedTabName}
                    todoList={todoList}
                />
            </View>
            {renderAddDialog()}
        </>
    )
}

const styles = StyleSheet.create({
    app: {
        flex: 1,
        backgroundColor: '#F9F9F9',
        padding: 15,
    },
    cardItem: {
        marginBottom: 15,
    },
    header: { flex: 1 },
    body: { flex: 5 },
    footer: { backgroundColor: 'white', height: 70 },
})
