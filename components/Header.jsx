import { Image, StyleSheet, Text } from 'react-native'
import logoImage from '../assets/logo.png'

export function Header() {
    return (
        <>
            <Image
                style={styles.image}
                source={logoImage}
                resizeMode="contain"
            />
            <Text style={styles.subtitle}>
                You probably have something to do
            </Text>
        </>
    )
}

const styles = StyleSheet.create({
    image: {
        width: 170,
    },
    subtitle: {
        marginTop: -20,
        fontSize: 20,
        color: '#ABABAB',
    },
})
