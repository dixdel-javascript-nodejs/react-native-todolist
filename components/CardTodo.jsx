import { Image, StyleSheet, Text, TouchableOpacity } from 'react-native'
import checkImage from '../assets/check.png'

export function CardTodo({ todo, onPress, onLongPress }) {
    return (
        <TouchableOpacity
            style={styles.card}
            onPress={() => onPress(todo)}
            onLongPress={() => onLongPress(todo)}
        >
            <Text
                style={[
                    styles.title,
                    todo.isCompleted && { textDecorationLine: 'line-through' },
                ]}
            >
                {todo.title}
            </Text>
            {todo.isCompleted && (
                <Image style={styles.image} source={checkImage} />
            )}
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    card: {
        backgroundColor: 'white',
        height: 115,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderRadius: 13,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
    },
    title: {
        fontSize: 25,
    },
    image: {
        height: 25,
        width: 25,
    },
})
